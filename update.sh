update()
{
    link=$1
    name=$2
    file=$(basename ${link})

    wget ${link}
    shasum=$(sha256sum ${file} | cut -d' ' -f1)
    echo "${link} ${shasum}" > ${name}
}
