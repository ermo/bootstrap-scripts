name        : llvm
version     : 13.0.0
release     : 1
summary     : Low Level Virtual Machine
license     :
    - Apache-2.0 WITH LLVM-exception
    - NCSA
homepage    : https://www.llvm.org
description : |
    Low Level Virtual Machine
upstreams   :
    - https://github.com/llvm/llvm-project/releases/download/llvmorg-13.0.0/llvm-project-13.0.0.src.tar.xz : 6075ad30f1ac0e15f07c1bf062c1e1268c241d674f11bd32cdf0e040c71f2bf3
environment : |
    export PATH="%(pkgdir)/bootstrap/bin:${PATH}"

    export llvmopts="
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_AR=%(pkgdir)/bootstrap/bin/llvm-ar \
        -DCMAKE_NM=%(pkgdir)/bootstrap/bin/llvm-nm \
        -DCMAKE_RANLIB=%(pkgdir)/bootstrap/bin/llvm-ranlib \
        -DCMAKE_C_COMPILER=%(pkgdir)/bootstrap/bin/clang \
        -DCMAKE_CXX_COMPILER=%(pkgdir)/bootstrap/bin/clang++ \
        -DCLANG_TABLEGEN=%(pkgdir)/bootstrap/bin/clang-tblgen \
        -DLLVM_TABLEGEN=%(pkgdir)/bootstrap/bin/llvm-tblgen \
        -DCLANG_BUILD_TOOLS=OFF \
        -DCLANG_DEFAULT_CXX_STDLIB=libc++ \
        -DCLANG_DEFAULT_LINKER=lld \
        -DCLANG_DEFAULT_OBJCOPY=llvm-objcopy \
        -DCLANG_DEFAULT_RTLIB=compiler-rt \
        -DCLANG_DEFAULT_UNWINDLIB=libunwind \
        -DCOMPILER_RT_USE_BUILTINS_LIBRARY=ON \
        -DCOMPILER_RT_USE_LIBCXX=ON \
        -DENABLE_LINKER_BUILD_ID=ON \
        -DLLVM_BUILD_TOOLS=ON \
        -DLLVM_DEFAULT_TARGET_TRIPLE=%(build) \
        -DLLVM_ENABLE_FFI=ON \
        -DLLVM_ENABLE_LIBCXX=ON \
        -DLLVM_ENABLE_LIBXML2=ON \
        -DLLVM_ENABLE_LTO=Thin \
        -DLLVM_ENABLE_PROJECTS='clang;compiler-rt;lld;llvm' \
        -DLLVM_INCLUDE_BENCHMARKS=OFF \
        -DLLVM_INCLUDE_TESTS=ON \
        -DLLVM_INCLUDE_UTILS=ON \
        -DLLVM_LIBDIR_SUFFIX=%(libsuffix) \
        -DLLVM_TARGET_ARCH=X86_64 \
        -DLLVM_TARGETS_TO_BUILD=X86 \
        -DLLVM_USE_SANITIZER=OFF \
        "
    cd llvm
setup       : |
    cd ..
    %patch %(pkgdir)/serpent/0001-Use-correct-Serpent-OS-multilib-paths-for-ld.patch
    %patch %(pkgdir)/serpent/0001-Make-gnu-hash-the-default-for-lld-and-clang.patch
    %patch %(pkgdir)/serpent/0001-Update-binutils-version-to-match-Serpent-OS.patch

    # Remove profile flags for bootstrap - this only works if this is last (it should be)
    export CFLAGS="${CFLAGS%%-fprofile-generate=*IR}"
    export CXXFLAGS="${CXXFLAGS%%-fprofile-generate=*IR}"
    export LDFLAGS="${LDFLAGS%%-fprofile-generate=*IR}"

    # Build a bootstrap stage1 compiler if it doesn't exist already
    if [ ! -d "%(pkgdir)/bootstrap" ]; then
        mkdir -p %(pkgdir)/bootstrap
        cd %(pkgdir)/bootstrap
        cmake -G Ninja %(workdir)/llvm \
            -DCMAKE_BUILD_TYPE=Release \
            -DCLANG_DEFAULT_LINKER=lld \
            -DCLANG_DEFAULT_OBJCOPY=llvm-objcopy \
            -DENABLE_LINKER_BUILD_ID=ON \
            -DLLVM_DEFAULT_TARGET_TRIPLE='%(build)' \
            -DLLVM_ENABLE_LTO=Thin \
            -DLLVM_ENABLE_PROJECTS='clang;lld;llvm' \
            -DLLVM_ENABLE_RUNTIMES='compiler-rt' \
            -DLLVM_INCLUDE_TESTS=OFF \
            -DLLVM_TARGET_ARCH='X86_64' \
            -DLLVM_TARGETS_TO_BUILD='host' \
            -DLLVM_OPTIMIZED_TABLEGEN=ON
        ninja -j "%(jobs)" clang compiler-rt lld llvm-profdata llvm-ar llvm-nm llvm-ranlib llvm-objcopy -v
    fi
build       : |
    # Separate the cmake stage from bootstrap for clean FLAGS
    %cmake ${llvmopts} \
        -DLLVM_BUILD_LLVM_DYLIB=ON \
        -DLLVM_LINK_LLVM_DYLIB=ON
    cd ..

    %cmake_build
install     : |
    %cmake_install

    %cmake ${llvmopts} \
        -DLLVM_BUILD_LLVM_DYLIB=OFF \
        -DLLVM_LINK_LLVM_DYLIB=OFF \
        -DCLANG_LINK_CLANG_DYLIB=OFF
    cd ..

    # Relink lld and clang statically
    %cmake_build lld clang
    %install_exe -t %(installroot)/usr/bin %(builddir)/bin/clang %(builddir)/bin/lld

    rm %(installroot)/%(libdir)/*.a
workload    : |
    # check-compiler-rt needs sanitizer changes
    %cmake_build check-clang check-llvm check-lld -k 10000 ||:
# LTO needs to be set in LLVM cmake flags so it adds -fno-lto where needed
tuning      :
    - fortify: false
    - harden: false
    - icf: all
    - lto: false
    - nosemantic
    - symbolic
cspgo       : false
