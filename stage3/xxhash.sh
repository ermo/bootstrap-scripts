#!/bin/true
set -e

. $(dirname $(realpath -s $0))/common.sh

extractSource xxhash
serpentChrootCd xxHash-*


printInfo "Building xxhash"
serpentChroot make PREFIX=/usr -j "${SERPENT_BUILD_JOBS}"

printInfo "Installing xxhash"
serpentChroot make PREFIX=/usr -j "${SERPENT_BUILD_JOBS}" install
